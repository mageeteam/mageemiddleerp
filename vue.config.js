// eslint-disable-next-line no-unused-vars
const path = require('path');
// eslint-disable-next-line no-unused-vars
const webpack = require('webpack');
module.exports = {
  lintOnSave: false,
  // // transpileDependencies:[/node_modules[/\\\\]vuetify[/\\\\]/],
  // transpileDependencies: ['vuex-persist'],
  publicPath: process.env.NODE_ENV === 'production' ? '/' : '/',
  pwa: {
    // configure the workbox plugin
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      // swSrc: 'public/service-worker.js',
      // ...other Workbox options...
      // swSrc: './src/sw.js',
      // swDest: 'service-worker.js',
    },
  },
  configureWebpack: {
    // entry:['@babel/polyfill','./src/main.js'],
    // resolve: {
    //   extensions: ['.js'],
    //   alias: {
    //     'jquery': 'jquery/dist/jquery.slim.js',
    //   }
    // },
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
      })
    ]
  },

  pluginOptions: {
    moment: {
      locales: [''],
    },
    // extends: ['plugin:vue/base'],
  },

  devServer: {
    open: process.platform === 'darwin',
    // host: '0.0.0.0',
    port: 7000, // CHANGE YOUR PORT HERE!
    https: false,
    hotOnly: false,
  },
};
