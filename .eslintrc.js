module.exports = {
  //此項是用來告訴eslint找當前配置檔案不能往父級查詢
  root: true,
  //此項是用來指定eslint解析器的，解析器必須符合規則，babel-eslint解析器是對babel解析器的包裝使其與ESLint解析
  parser: 'babel-eslint',
  //此項是用來指定javaScript語言型別和風格，sourceType用來指定js匯入的方式，預設是script，此處設定為module，指某塊匯入方式
  parserOptions: {
    sourceType: 'module'
  },
  //此項指定環境的全域性變數，下面的配置指定為瀏覽器環境
  env: {
    browser: true,
  },
  // https://github.com/feross/standard/blob/master/RULES.md#javascript-standard-style
  // 此項是用來配置標準的js風格，就是說寫程式碼的時候要規範的寫，如果你使用vs-code我覺得應該可以避免出錯
  // extends: 'standard',
  // required to lint *.vue files
  // 此項是用來提供外掛的，外掛名稱省略了eslint-plugin-，下面這個配置是用來規範html的
  plugins: [
    // 'html'
  ],
  // add your custom rules here
  // 下面這些rules是用來設定從外掛來的規範程式碼的規則，使用必須去掉字首eslint-plugin-
  // 主要有如下的設定規則，可以設定字串也可以設定數字，兩者效果一致
  // "off" -> 0 關閉規則
  // "warn" -> 1 開啟警告規則
  //"error" -> 2 開啟錯誤規則
  // 瞭解了上面這些，下面這些程式碼相信也看的明白了
  'rules': {
    // allow paren-less arrow functions
    'arrow-parens': 0,
    // allow async-await
    'generator-star-spacing': 0,
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
  }
};
