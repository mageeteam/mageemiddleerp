import Vue from 'vue';
import VueRouter from 'vue-router';
import _ from "lodash";
import store from "@/store";
// import newletterExport from '../views/newletterExport.vue';
// import Home from '../views/Home.vue';

Vue.use(VueRouter);

const getPage = (name, path = "views") => () => {
  return import(`@/${path}/${name}.vue`);
};
const pagedefault = (body, header = getPage("header", "components"), footer = getPage("footer",
  "components")) => {
  return {
    header: header,
    body: body,
    footer: footer
  };
}

const routes = [{
    path: "/Login",
    components: pagedefault(getPage('Login')),
  },
  {
    path: '/',
    meta: {
      Login: true
    },
    components: pagedefault(getPage('index')),
    children: [{
        path: '',
        name: 'index',
        component: getPage('myOrder', 'views'),
      },
      {
        path: 'order',
        name: 'myorder',
        component: getPage('myOrder', 'views'),
      },
      {
        path: 'productManage',
        component: getPage('productManagement', 'views'),
        children: [{
            path: 'proOverview',
            component: getPage('proOverview', 'views/proManage'),
          },
          {
            path: 'proEdit',
            component: getPage('proEdit', 'views/proManage'),
            children: [{
                path: 'proBaseField',
                component: getPage('proBaseField', 'views/proManage/proEdit'),
              },
              {
                path: 'proCost',
                component: getPage('proCost', 'views/proManage/proEdit'),
              },
              {
                path: 'proSpecification',
                component: getPage('proSpecification', 'views/proManage/proEdit'),
              },
              {
                path: 'proStock',
                component: getPage('proStock', 'views/proManage/proEdit'),
              },
              {
                path: 'proOther',
                component: getPage('proOther', 'views/proManage/proEdit'),
              },
            ],
          },
          {
            path: 'proUploadImg',
            component: getPage('proUploadImg', 'views/proManage'),
          },
          {
            path: 'proClassify',
            component: getPage('proClassify', 'views/proManage'),
          },
          {
            path: 'proSortClass',
            component: getPage('proSortClass', 'views/proManage'),
            // children: [{
            //     path: 'newClass',
            //     component: getPage('newClass', 'components/proManage/proSortClass'),
            //   },
            // ],
          },
        ],
      },
      {
        path: 'shoppingCart',
        component: getPage('shoppingCart', 'views'),
        children: [{
            path: 'fullDiscount',
            component: getPage('fullDiscount', 'views/shoppingCart'),
          },
          {
            path: 'totalDiscount',
            component: getPage('totalDiscount', 'views/shoppingCart'),
          },
          {
            path: 'newDiscount',
            component: getPage('newDiscount', 'views/shoppingCart'),
          },
          {
            path: 'plus',
            component: getPage('plus', 'views/shoppingCart'),
          },
          {
            path: 'ifOpenShop',
            name: 'ifOpenShop',
            component: getPage('ifOpenShop', 'views/shoppingCart'),
          },
          {
            path: 'shopIntro',
            name: 'shopIntro',
            component: getPage('shopIntro', 'views/shoppingCart'),
          },
          {
            path: 'thanksText',
            name: 'thanksText',
            component: getPage('thanksText', 'views/shoppingCart'),
          },
          {
            path: 'shipMethod',
            name: 'shipMethod',
            component: getPage('shipMethod', 'views/shoppingCart'),
          },
          {
            path: 'payMethod',
            name: 'payMethod',
            component: getPage('payMethod', 'views/shoppingCart'),
          },
          {
            path: 'receiptSetting',
            name: 'receiptSetting',
            component: getPage('receiptSetting', 'views/shoppingCart'),
          },
          {
            path: 'cart_extraCost',
            name: 'cart_extraCost',
            component: getPage('cart_extraCost', 'views/shoppingCart'),
          },
          {
            path: 'cart_lowestLimit',
            name: 'cart_lowestLimit',
            component: getPage('cart_lowestLimit', 'views/shoppingCart')
          },
          {
            path: 'cart_orderField',
            name: 'cart_orderField',
            component: getPage('cart_orderField', 'views/shoppingCart'),
          }
        ],
      },
      {
        path: 'memberinformation',
        component: getPage('memberInfo', 'views'),
      },
      {
        path: 'changepassword',
        component: getPage('changePasswd', 'views'),
      },
      {
        path: 'courseleave',
        component: getPage('courseLeave', 'views'),
      },
      {
        path: 'onlineService',
        component: getPage('onlineService', 'views'),
      },
      {
        path: 'InsertProduct',
        component: getPage('InsertProduct', 'views'),
        children: [{
            path: 'insBaseField',
            component: getPage('insBaseField', 'views/InsertProduct'),
          },
          {
            path: 'insCost',
            component: getPage('insCost', 'views/InsertProduct'),
          },
          {
            path: 'insSpecification',
            component: getPage('insSpecification', 'views/InsertProduct'),
          },
          {
            path: 'insStock',
            component: getPage('insStock', 'views/InsertProduct'),
          },
          {
            path: 'insOther',
            component: getPage('insOther', 'views/InsertProduct'),
          },
        ],
      },
      {
        path: 'newsletter',
        name: 'newsletter',
        component: getPage('newsletter', 'views'),
      },
      {
        path: 'shipManage',
        component: getPage('shipManage', 'views'),
        children: [{
            path: 'shipBase',
            component: getPage('shipBase', 'views/ship'),
          },
          {
            path: 'shipTrack',
            component: getPage('shipTrack', 'views/ship'),
          },
        ],
      },
      {
        path: 'arrival',
        component: getPage('arrival', 'views'),
      },
      {
        path: 'printShip',
        component: getPage('printShip', 'views'),
        children: [{
            path: 'shipedList',
            component: getPage('shipedList', 'views/printShip'),
          },
          {
            path: 'waitConfirm',
            component: getPage('waitConfirm', 'views/printShip'),
          },
          {
            path: 'finish',
            component: getPage('finish', 'views/printShip'),
          },
        ],
      },
      // {
      //   path: '/newletterExport',
      //   component: getPage('newletterExport', 'views'),
      //   name: 'newletterExport',
      // },

      // {
      //   path: 'discount',
      //   component: getPage('discount', 'views'),
      //   children: [{
      //     path: 'fullDiscount',
      //     component: getPage('fullDiscount', 'views/discount'),
      //   },
      //   {
      //     path: 'totalDiscount',
      //     component: getPage('totalDiscount', 'views/discount'),
      //   },
      //   {
      //     path: 'newDiscount',
      //     component: getPage('newDiscount', 'views/discount'),
      //   },
      //   {
      //     path: 'plus',
      //     component: getPage('plus', 'views/discount'),
      //   }
      //   ],
      // },
    ],
  },
  {
    path: '/newletterExport',
    components: {
      newletterExport: getPage('newletterExport'),
    },
    name: 'newletterExport',
  },

  // {
  //   path: 'newsletter',
  //   name: 'newsletter',
  //   component: getPage('newsletter', 'views'),
  // },
  // {
  //   path: 'shipManage',
  //   component: getPage('shipManage', 'views'),
  //   children: [{
  //       path: 'shipBase',
  //       component: getPage('shipBase', 'views/ship'),
  //     },
  //     {
  //       path: 'shipTrack',
  //       component: getPage('shipTrack', 'views/ship'),
  //     },
  //   ],
  // },
  // {
  //   path: 'arrival',
  //   component: getPage('arrival', 'views'),
  // },
  // {
  //   path: 'printShip',
  //   component: getPage('printShip', 'views'),
  //   children: [{
  //       path: 'shipedList',
  //       component: getPage('shipedList', 'views/printShip'),
  //     },
  //     {
  //       path: 'waitConfirm',
  //       component: getPage('waitConfirm', 'views/printShip'),
  //     },
  //   ],
  // },
];

const router = new VueRouter({
  base: "/",
  mode: 'history',
  routes,
});
router.beforeEach(async (to, from, next) => {
  console.log(to, from);
  const checkmeber = to.matched;
  const ischeck = checkmeber.reduce((sum, item) => {
    return (sum) ? sum : !!item.meta.Login
  }, false);

  if (ischeck) {
    const check = await store.dispatch('Login/checkLogin');
    console.log(check);
    if (check) {
      // next({
      //   name: "Login"
      // });
      next();
    } else {
      next();
    }
  }
  next();
});

export default router;
