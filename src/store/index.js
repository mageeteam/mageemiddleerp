import Vue from 'vue';
import Vuex from 'vuex';
import productManage from './modules/productManage';
import productInsert from './modules/productInsert';
import discount from './modules/discount';
import Public from './modules/public';
import ship from './modules/ship';
import Login from './modules/Login';
import printShip from './modules/printShip';
import NewsLetter from './modules/NewsLetter';
import shoppingCart from './modules/shoppingCart';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    productManage,
    productInsert,
    discount,
    Public,
    ship,
    Login,
    printShip,
    NewsLetter,
    shoppingCart
  },
});
