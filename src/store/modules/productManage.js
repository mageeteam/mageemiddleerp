/* eslint-disable no-shadow */
const state = {
  fields: [
    { name: '訂單編號／流水號' },
    { name: '購買日期' },
    { name: '購買人' },
    { name: '送貨地址' },
    { name: '貨款狀態' },
    { name: '出貨狀態' },
    { name: '編輯訂單' },
    { name: '其他' },
    { name: '合計' },
  ],
  orderList: [
    {
      id: '20201',
      date: '2020-03-16',
      name: 'aaa',
      address: '台北',
      payment: '已付款',
      ship: '已出貨',
      edit: '已編輯',
      other: '無',
      total: 100,
    },
  ],
  proStyle: {
    proSort: '4',
    proOverViewWidth: '50',
    proTitleSize: '20',
    proTitleColor: '#000000',
    proPriceSize: '15',
    proPriceColor: '#000000',
    proTypePriceSize: '15',
    proTypePriceColor: '#000000',
    proInfoSize: '15',
    proInfoColor: '#000000',
    proImgBorder: false,
    proTempleStyle: 'vertical_1',
  },

};

const actions = {
};

const mutations = {
  // changeProSort(state, data) {
  //   state.proStyle.proSort = data;
  // },
  // changeProWidth(state, data) {
  //   state.proStyle.proOverViewWidth = data;
  // },
  // changeProTitle(state, data) {
  //   state.proStyle.proTitleSize = data;
  // },
  // changeProTitleColor(state, data) {
  //   state.proStyle.proTitleColor = data;
  // },
  // changeProPriceSize(state, data) {
  //   state.proStyle.proPriceSize = data;
  // },
  // changeProPriceColor(state, data) {
  //   state.proStyle.proPriceColor = data;
  // },
  changeProStyle(state, data) {
    state.proStyle.proSort = data.proSort;
    state.proStyle.proOverViewWidth = data.proOverViewWidth;
    state.proStyle.proTitleSize = data.proTitleSize;
    state.proStyle.proTitleColor = data.proTitleColor;
    state.proStyle.proPriceSize = data.proPriceSize;
    state.proStyle.proPriceColor = data.proPriceColor;
    state.proStyle.proTypePriceSize = data.proTypePriceSize;
    state.proStyle.proTypePriceColor = data.proTypePriceColor;
    state.proStyle.proInfoSize = data.proInfoSize;
    state.proStyle.proInfoColor = data.proInfoColor;
    state.proStyle.proImgBorder = data.proImgBorder;
    state.proStyle.proTempleStyle = data.proTempleStyle;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
