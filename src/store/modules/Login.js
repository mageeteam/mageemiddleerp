/* eslint-disable no-shadow */
import moment from "moment";
const state = {
  check: false,
  member: {
    name: "",
    email: "",
    phone: "",
  },
  api_token: "",
  LoginTime: "",
  Company: "",
  CompanyImage: "LOGO.ae1aaf32.png"
};
const getters = {

}
const actions = {
  Login({
    commit
  }, data) {
    commit("setMemberData", {
      api_token: "hello",
      name: "周信榮",
      email: "james60713@gmail.com",
      phone: "0921467841"
    });
  },
  async checkLogin({
    commit
  }) {
    let check = {}
    try {
      check = await new Promise((resolve) => {
        const data = {
          status: true,
          data: {
            api_token: "123456789"
          }
        };
        resolve({ data :data});
      });
      commit("checkLogin", check.data.status);
      commit("setApiToken", check.data.data.api_token);
    } catch (error) {
      console.log(error);
      check.data.status = false;
    }
    return check.data.status;
    // console.log("B",sa);

  }
};

const mutations = {
  setCompany(state, data) {
    state.Company = data;
  },
  setApiToken(state, data) {
    state.api_token = data;
    sessionStorage.Authorization = state.api_token;
  },
  checkLogin(state, data) {
    state.check = data;
  },
  setMemberData(state, data) {
    state.Login = data.state;
    if (state.Login) {
      state.member = {
        name: data.name,
        email: data.email,
        phone: data.phone,
      };
      state.api_token = data.api_token;
      state.LoginTime = moment();
      sessionStorage.Authorization = state.api_token;
    } else {
      state.member = {
        name: "",
        email: "",
        phone: "",
      };
      state.Authorization = data.api_token;
      state.LoginTime = null;
    }
  },

};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
