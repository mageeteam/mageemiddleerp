/* eslint-disable no-shadow */
const state = {
  nowSpecification: 'single',
};

const actions = {

};

const mutations = {
  setSpecification(state, data) {
    state.nowSpecification = data;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
