/* eslint-disable no-shadow */
const state = {
  lang: 'zh-tw',
  langtext: {
    dis_list: {
      off: '折價',
      discount: '折扣',
      true: '是',
      false: '否',
    },
    stock: {
      
    }
  },
};

const actions = {

};

const mutations = {

};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
