/* eslint-disable no-shadow */
const state = {
  editdata: {
    dis_name: '',
    dis_intro: '',
  },

  edit_dis_list: {},
  dis_code_list: [
    {
      dis_from: '折扣區',
      title: '紅標區',
      dis_code: '2020',
      off: 200,
      condition: [
        5, 10, 15,
      ],
    },
    {
      dis_from: '商品區',
      title: 'A0001',
      dis_code: '12ba7w',
      off: 100,
      condition: [
        1,
      ],
    },
  ],
  dis_list: [
    {
      title: '紅標區',
      uid: 'XXXXXX',
      extra: true,
      sub: [
        {
          name: '黃標區',
          off: 500,
          method: 'off',
          enable: false,
        },
        {
          name: '藍標區',
          off: 2600,
          method: 'off',
          enable: true,
        },
      ],
    }, {
      title: '藍標區',
      uid: 'XXXXXX',
      extra: true,
      sub: [
        {
          name: '紅標區',
          off: 500,
          method: 'off',
          enable: true,
        },
        {
          name: '藍標區',
          off: 8,
          method: 'discount',
          enable: true,
        },
      ],
    },
  ],
  buyfullDisData: [{
    dis_name: '紅標區',
    dis_intro: '新年度三科班學員',
    dis_isshow: true,
    dis_date_start: ['2020/03/20', '24:00'],
    dis_date_end: ['2020/03/25', '24:00'],
    dis_date_type: 'in_date',
    dis_authority_type: 'showFull',
    dis_authority_type2: 'cumulate',
    dis_authority_disable: [],
    dis_type: 'discount_price',
    dis_num: [{
      type: 'piece',
      full_num: 5,
      discount: 500,
    },
    {
      type: 'piece',
      full_num: 10,
      discount: 800,
    },
    {
      type: 'piece',
      full_num: 15,
      discount: 1000,
    },
    ],
    dis_mix: ['can_mix'],
    dis_code: [true, '2020'],
    dis_show_other: true,
    dis_show_extra: true,
    dis_extra_setting: {
      extra: true,
      extra_data: [{
        index: 1,
        name: '黃標區',
        method: 'off',
        discount: 500,
        enable: true,
        addpro: false,
      },
      {
        index: 2,
        name: '藍標區',
        method: 'off',
        discount: 2600,
        enable: true,
        addpro: false,
      },
      ],
    },
  },
    // {
    //   dis_name: '紅標區',
    //   dis_intro: '新年度三科班學員',
    //   dis_isshow: true,
    //   dis_date_start: ['2020/03/20', '24:00'],
    //   dis_date_end: ['2020/03/25', '24:00'],
    //   dis_date_type: 'in_date',
    //   dis_authority_type: 'showFull',
    //   dis_authority_type2: 'cumulate',
    //   dis_authority_disable: [],
    //   dis_type: 'discount_price',
    //   dis_num: [{
    //     type: 'piece',
    //     full_num: 5,
    //     discount: 500,
    //   },
    //   {
    //     type: 'piece',
    //     full_num: 10,
    //     discount: 800,
    //   },
    //   {
    //     type: 'piece',
    //     full_num: 15,
    //     discount: 1000,
    //   },
    //   ],
    //   dis_mix: ['can_mix'],
    //   dis_code: [true, '2020'],
    //   dis_show_other: false,
    //   dis_extra_setting: {
    //     extra: true,
    //     extra_data: [{
    //       index: 1,
    //       name: '黃標區',
    //       method: 'off',
    //       discount: 500,
    //       enable: true,
    //     },
    //     {
    //       index: 2,
    //       name: '藍標區',
    //       method: 'off',
    //       discount: 2600,
    //       enable: true,
    //     },
    //     ],
    //   },
    // },

  ],
};

const actions = {};

const mutations = {
  change_edit_dis_list(state, data) {
    console.log(data);
    state.edit_dis_list = data;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
