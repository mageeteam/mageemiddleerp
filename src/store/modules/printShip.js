const state = {
  stock2: {
    voice: {
      name: "有聲書（九版）",
      stock: 20,
      ship_count: 4,
    },
    eng: {
      name: "英語篇（九版）",
      stock: 0,
      ship_count: 1,
    },
    jap: {
      name: "日語篇（九版）",
      stock: 10,
      ship_count: 1,
    }
  },
  stock: [{
    name: "有聲書（九版）",
    type: 'voice',
    stock: 20,
    ship_count: 4,
  },
  {
    name: "英語篇（九版）",
    type: 'eng',
    stock: 0,
    ship_count: 1,
  },
  {
    name: "日語篇（九版）",
    type: 'jap',
    stock: 10,
    ship_count: 1,
  }
  ],
  preview_post_fin: [],
  preview_on_ship_fin: [],
  preview_cvs_fin: [],
  preview_on_site_fin: [],



  preview_post: [],
  preview_on_ship: [],
  preview_cvs: [],
  preview_on_site: [],
  print_ship_list: {
    post_office: [{
      uid: "0106310044778",
      post_code: '',
      method: "post_office",
      date: "2020-04-08",
      date_Interval: "all_day",
      purchaser: "BBB",
      pay_status: "paid",
      phone: "0912345678",
      address: "106台北市大安區成功郵局ＯＯ號信箱",
      receipt: "no_print",
      list: [{
        name: "有聲書（九版）",
        count: 2
      },
      {
        name: "四科套書（九版）",
        count: 2
      }
      ]
    },
    {
      uid: "0106310044779",
      post_code: '',
      method: "post_office",
      date: "2020-04-08",
      date_Interval: "all_day",
      purchaser: "aaa",
      pay_status: "paid",
      phone: "0987654321",
      address: "106台北市大安區ＯＯ郵局第ＯＯ號信箱",
      receipt: "is_print",
      list: [{
        name: "有聲書（九版）",
        count: 1
      }]
    }
    ],
    on_delivery: [{
      uid: "0106310044780",
      check_id: '',
      method: "on_delivery",
      date: "2020-04-08",
      date_Interval: "night",
      purchaser: "ccc",
      pay_status: "no_pay",
      phone: "0955511234",
      address: "106台北市大安區ＯＯＯ路ＯＯ號ＯＯ樓",
      receipt: "no_print",
      list: [{
        name: "英語篇（九版）",
        count: 1
      }]
    }],
    cvs: [{
      uid: "0106310044781",
      check_id: '',
      method: "cvs",
      date: "2020-04-08",
      date_Interval: "night",
      purchaser: "ddd",
      pay_status: "no_pay",
      phone: "0955511234",
      address: "106台北市大安區ＯＯＯ路ＯＯ號ＯＯ樓",
      receipt: "no_print",
      list: [{
        name: "英語篇（九版）",
        count: 1
      }]
    }],
    on_site: [{
      uid: "0106310044782",
      check_id: '',
      method: "on_site",
      date: "2020-04-08",
      date_Interval: "night",
      purchaser: "eee",
      pay_status: "no_pay",
      phone: "0955511234",
      address: "106台北市大安區ＯＯＯ路ＯＯ號ＯＯ樓",
      receipt: "no_print",
      list: [{
        name: "英語篇（九版）",
        count: 1
      }]
    }]
  }
  // print_ship_list: [{
  //     uid: "",
  //     method: "post_office",
  //     date: "2020-04-08",
  //     date_Interval: "all_day",
  //     purchaser: "BBB",
  //     pay_status: "paid",
  //     phone: "0912345678",
  //     address: "106台北市大安區成功郵局ＯＯ號信箱",
  //     receipt: "no_print",
  //     list: [{
  //         name: "有聲書（九版）",
  //         count: 2
  //       },
  //       {
  //         name: "四科套書（九版）",
  //         count: 2
  //       }
  //     ]
  //   },
  //   {
  //     uid: "",
  //     method: "post_office",
  //     date: "2020-04-08",
  //     date_Interval: "all_day",
  //     purchaser: "aaa",
  //     pay_status: "paid",
  //     phone: "0987654321",
  //     address: "106台北市大安區ＯＯ郵局第ＯＯ號信箱",
  //     receipt: "is_print",
  //     list: [{
  //       name: "有聲書（九版）",
  //       count: 1
  //     }]
  //   },
  //   {
  //     uid: "",
  //     method: "on_delivery",
  //     date: "2020-04-08",
  //     date_Interval: "night",
  //     purchaser: "ccc",
  //     pay_status: "no_pay",
  //     phone: "0955511234",
  //     address: "106台北市大安區ＯＯＯ路ＯＯ號ＯＯ樓",
  //     receipt: "no_print",
  //     list: [{
  //       name: "英語篇（九版）",
  //       count: 1
  //     }]
  //   }
  // ]
};

const actions = {
  delPreviewPost({ state, commit }, data) {
    if (data.method === "post_office") {
      const newArr2 = state.preview_post.filter(e => e.uid === data.uid);
      commit("savePreviewPostFin", newArr2);
      const newArr = state.preview_post.filter(e => e.uid !== data.uid);
      commit("savePreviewPost", newArr);
    } else if (data.method === "on_delivery") {
      const newArr2 = state.preview_on_ship.filter(e => e.uid === data.uid);
      commit("savePreviewOnFin", newArr2);
      const newArr = state.preview_on_ship.filter(e => e.uid !== data.uid);
      commit("savePreviewOn", newArr);
    } else if (data.method === "cvs") {
      const newArr2 = state.preview_cvs.filter(e => e.uid === data.uid);
      commit("savePreviewCvsFin", newArr2);
      const newArr = state.preview_cvs.filter(e => e.uid !== data.uid);
      commit("savePreviewCvs", newArr);
    } else if (data.method === "on_site") {
      const newArr2 = state.preview_on_site.filter(e => e.uid === data.uid);
      commit("savePreviewOnSiteFin", newArr2);
      const newArr = state.preview_on_site.filter(e => e.uid !== data.uid);
      commit("savePreviewOnSite", newArr);
    }
  }
};

const mutations = {
  savePreviewPostFin(state, data) {
    state.preview_post_fin.push(...data);
  },
  savePreviewOnFin(state, data) {
    state.preview_on_ship_fin.push(...data);
  },
  savePreviewCvsFin(state, data) {
    state.preview_cvs_fin.push(...data);
  },
  savePreviewOnSiteFin(state, data) {
    state.preview_on_site_fin.push(...data);
  },
  savePreviewPost(state, data) {
    // state.preview_post = [];
    state.preview_post = data;
  },
  savePreviewOn(state, data) {
    // state.preview_on_ship = [];
    state.preview_on_ship = data;
  },
  savePreviewCvs(state, data) {
    // state.preview_on_ship = [];
    state.preview_cvs = data;
  },
  savePreviewOnSite(state, data) {
    // state.preview_on_ship = [];
    state.preview_on_site = data;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
