/* eslint-disable no-shadow */
const state = {
  shipMethod_list: [{
      status: '(未啟用)',
      isezShip: false,
      name: '7-11取貨付款',
      img: 'https://img1.shop2000.com.tw/image/sys/7-11.jpg',
      cost: 0,
      info: [
        '貨品到達超商時會有Email與簡訊通知您到超商取貨',
        '外島地區消費者需額外支付40元',
        '收貨人姓名、手機、Email請務必正確填寫',
        '取貨付款時，不需出示證件便可取貨付款',
        '貨品於超商保留7天，逾期退回',
        '訂單最高金額不可大於兩萬'
      ],
      env: {
        status: true,
        setting: {
          info: '尚未設定'
        }
      }
    },
    {
      status: '(未啟用)',
      isezShip: false,
      name: '7-11取貨不付款',
      img: 'https://img1.shop2000.com.tw/image/sys/7-11.jpg',
      cost: 0,
      info: [
        '貨品到達超商時會有Email與簡訊通知您到超商取貨',
        '外島地區消費者需額外支付40元',
        '收貨人姓名、手機、Email請務必正確填寫',
        '純取貨時，需出示證件才能取貨，故上方表格須填取貨人真實姓名',
        '貨品於超商保留7天，逾期退回',
        '訂單最高金額不可大於兩萬'
      ],
      env: {
        status: false,
        setting: {
          info: '尚未設定'
        }
      }
    },
    {
      status: '(未啟用)',
      isezShip: true,
      name: 'ezShip超商取貨',
      img: 'https://img1.shop2000.com.tw/image/sys/3store.jpg',
      cost: 0,
      info: [
        'ＯＫ、萊爾富、全家 取貨',
        '貨品到達超商時會有Email與簡訊通知您到超商取貨',
        '收貨人姓名、手機、Email請務必正確填寫',
        '純取貨時，需出示證件才能取貨，故上方表格須填取貨人真實姓名',
        '貨品於超商保留7天，逾期退回',
        '訂單最高金額不可大於兩萬',
        '本服務由ezShip所提供'
      ],
      env: {
        status: false,
        setting: {
          info: '尚未設定'
        }
      }
    }
  ],
  payMethod_list: [{
      status: '(未啟用)',
      method: '金融匯款',
      img: '',
      info: {
        type: 'less',
        list: ['匯款完畢後，可於系統發出的訂單通知信內或登入會員，點選 [訂單後續連絡] 來發送匯款完成通知，以便對帳出貨']
      },
      env: {
        method: ''
      }
    },
    {
      status: '(未啟用)',
      method: '7-11列印繳費',
      img: 'https://img1.shop2000.com.tw/image/sys/7-11ibon.jpg',
      info: {
        type: 'table',
        title: '請到7-11店內的ibon機台上',
        list: ['點 輸入代碼-->固定輸入 LCV', '再輸入 ibon繳費代碼xxxxxxxxx ( 共九碼 )', '便可列印出小白單，拿到櫃台付款'],
        link: {
          title: '看ibon操作畫面',
          url: ''
        },
        list2: ['購物結束頁面可看到完整繳費代碼', '最少100元，最多二萬元']
      },
      env: {
        method: '設定收款銀行'
      }
    },
    {
      status: '(未啟用)',
      method: '全家列印繳費',
      img: 'https://img1.shop2000.com.tw/image/sys/famiport.jpg',
      info: {
        type: 'table',
        title: '請到全家超商內的FamiPort機台上',
        list: ['點 繳費-->虛擬帳號-->廠商代碼固定輸入：SHOP2000 (大寫)', '再輸入店家編號 00108550 ( 共八碼 )',
          '再輸入訂單編號 xxxxxxxx ( 共八碼 )', '再輸入繳費金額 xxxx', '便可列印出小白單，拿到櫃台付款',
        ],
        link: {
          title: '看FamiPort操作畫面',
          url: ''
        },
        list2: ['購物結束頁面可看到完整繳費代碼', '最少100元，最多二萬元']
      },
      env: {
        method: '輸入payNow參數'
      }
    },
    {
      status: '(未啟用)',
      method: 'ATM轉帳(虛擬帳號)',
      img: '',
      info: {
        type: 'li',
        list: ['於購物結束頁面您會看到一組本次交易專屬的匯款帳號 (即虛擬帳號)，用紙筆抄下後，到ATM提款機上(或於個人電腦使用讀卡機) 輸入此虛擬帳號進行匯款',
          '於ATM上操作的方式與一般金融匯款相同，金額必須輸入正確否則無法成功匯款',
          '請於 5 日 內進行繳費，超過期限此虛擬帳號即失效', '虛擬帳號不可於銀行櫃檯臨櫃匯款', '忘記虛擬帳號時，您可登入會員或於訂單通知信內查詢此虛擬帳號',
        ],
      },
      env: {
        method: '輸入payNow參數'
      }
    },
    {
      status: '(未啟用)',
      method: 'payNow線上刷卡',
      img: '',
      info: {
        type: null,
      },
      env: {
        method: '輸入payNow參數'
      }
    },
    {
      status: '(未啟用)',
      method: 'webATM匯款',
      img: 'https://img1.shop2000.com.tw/image/sys/cardreader.jpg',
      info: {
        type: 'less',
        list: ['使用USB讀卡機並插入您的晶片金融卡，進行webATM匯款', '金融單位會收取您轉帳手續費17元，即使非跨行也會收取'],
      },
      env: {
        method: '設定收款銀行'
      }
    }
  ],
  orderField: [
    {
      title: '姓名',
      type: '文字框',
      config: {
        type: 'text',
        mustFill: '(必填)',
      }
    },
    {
      title: '性別',
      type: '單選',
      config: {
        type: 'checkbox',
        mustFill: '必填',
      }
    },
    {
      title: '手機',
      type: '文字框',
      config: {
        type: 'both',
        mustFill: '(必填)',
        checttext: '鎖定10碼',
      }
    },
    {
      title: '電話',
      type: '文字框',
      config: {
        type: 'checkbox',
        mustFill: '必填',
      }
    },
    {
      title: 'Email',
      type: '文字框',
      config: {
        type: 'checkbox',
        mustFill: '必填',
      }
    },
    {
      title: '送貨地址',
      type: 'Address',
      type_list: [
        {
          radioBtn: '下拉縣市 + 鄉鎮市',
          text:'台灣適用，運費會依北中南部寫入訂單'
        },
        {
          radioBtn: '單一欄位輸入框',
          text:'國外適用，運費預設為空，訂單成立後店家手動輸入運費'
        }
      ],

      config: {
        type: 'text',
        mustFill: '必填(超商取貨時非必填)',
      }
    },
    {
      title: '買方備註',
      type: '大文字框',
      config: {
        type: 'text',
        mustFill: '選填',
      }
    },
    {
      title: '訂閱電子報',
      type: '核取框',
      config: {
        type: 'checkbox',
        mustFill: '啟用',
      }
    },
  ]
};

const actions = {

};

const mutations = {

};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
