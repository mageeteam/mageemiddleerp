const state = {
  editNow:0,
  style: {
    localStyle: {
      Horizontal_style: {

      },
      Content1_style: {
        // bg_color: '#FFFFFF',
        // text_color: '#000000',
        // title_color:'#ab7c14',
      },
      Content2_style: {
        // h1_color:'#000000',
        // bg_color: '#FFFFFF',
        // text_color: '#000000',
        // title_color:'#000000',
      },
      Content2_2_style: {
        // h1_color:'#000000',
        // bg_color: '#FFFFFF',
        // text_color: '#000000',
        // title_color: '#ab7c14',
        // btn_color: '#ab7c14',
        // btn_border: 1,
      },
      Content3_style: {
        // h1_color:'#000000',
        // bg_color: '#FFFFFF',
        // text_color: '#000000',
        // title_color: '#ab7c14',
        // btn_color: '#ab7c14',
        // btn_border: 1,
      },
      Content4_style: {
        li_style:'disc',
      }
    },
    globalStyle: {
      // bg_color: '#ffc68c',
      switchStyle:'newLetterContent3',
    }
  },
  newsFooter: {
    info: [{
        spantext: '01',
        url: 'https://forms.gle/TwUpyzDN3DVx6nML7'
      },
      {
        spantext: '02',
        url: 'https://forms.gle/TwUpyzDN3DVx6nML7'
      },
      {
        spantext: '03',
        url: 'https://forms.gle/TwUpyzDN3DVx6nML7'
      }
    ],
    coName: 'Mageecube Company',
  },
  Logo: {
    img: '',
    coName: 'Mageecube Company',
  },
  Content2: [{
    h1text: 'H1 主標題',
    info: [{
        h2text: '【現場上課】',
        ptext: '課程日期： 109/4/25(六)◎ 場次一：12:30-16:30◎ 場次二：17:30-21:30每場限額：30名課程時數：4小時課程費用：免費報名期間：109/4/21～4/24',
        btntext: '電話報名0227338118',
        img: '',
        showImg: true,
        url: 'tel:0227338118',
      },
      {
        h2text: '【雲端上課】',
        ptext: '課程日期：109/4/30～5/9課程時數：4小時課程費用：免費報名期間：109/4/27～5/5',
        btntext: '線上報名',
        img: '',
        showImg: true,
        url: 'https://forms.gle/TwUpyzDN3DVx6nML7',
      },
      // {
      //   h2text: '【雲端上課】2',
      //   ptext: '課程日期：109/4/30～5/9課程時數：4小時課程費用：免費報名期間：109/4/27～5/5',
      //   btntext: '線上報名',
      //   img: '',
      //   showImg: true,
      //   url: 'https://forms.gle/TwUpyzDN3DVx6nML7',
      // },
      // {
      //   h2text: '【雲端上課】2',
      //   ptext: '課程日期：109/4/30～5/9課程時數：4小時課程費用：免費報名期間：109/4/27～5/5',
      //   btntext: '線上報名',
      //   img: '',
      //   showImg: true,
      //   url: 'https://forms.gle/TwUpyzDN3DVx6nML7',
      // },
    ],
  }, ],
  Content2_2: [{
    h1text: 'H1 主標題',
    info: [
      // [{
      //     h2text: '入場證下載',
      //     ptext: '考試通知書預定於109/4/24起開放考生自行下載。考試當日，應考生需持身分證件(國民身分證或護照或我國居留證) 入場應試。 ',
      //     btntext: '考試通知書下載',
      //     img: '',
      //     showImg: true,
      //     url: 'https://register.moex2.nat.gov.tw/index.html',
      //   },
      //   {
      //     h2text: '考試日程',
      //     ptext: '領隊考試：109／5／9（六） 導遊考試：109／5／10（六）',
      //     btntext: '考程表下載',
      //     img: '',
      //     showImg: true,
      //     url: 'https://www.magee.tw/docs/exam_notes/48',
      //   }
      // ],
      // [
      //   {
      //     h2text: '入場證下載',
      //     ptext: '考試通知書預定於109/4/24起開放考生自行下載。考試當日，應考生需持身分證件(國民身分證或護照或我國居留證) 入場應試。 ',
      //     btntext: '考試通知書下載',
      //     img: '',
      //     showImg: true,
      //     url: 'https://register.moex2.nat.gov.tw/index.html',
      //   },
      //   {
      //     h2text: '考試日程',
      //     ptext: '領隊考試：109／5／9（六） 導遊考試：109／5／10（六）',
      //     btntext: '考程表下載',
      //     img: '',
      //     showImg: true,
      //     url: 'https://www.magee.tw/docs/exam_notes/48',
      //   }
      // ]
      {
        rowNum: 0,
        h2text: '入場證下載',
        ptext: '考試通知書預定於109/4/24起開放考生自行下載。考試當日，應考生需持身分證件(國民身分證或護照或我國居留證) 入場應試。 ',
        btntext: '考試通知書下載',
        img: '',
        showImg: true,
        url: 'https://register.moex2.nat.gov.tw/index.html',
      },
      {
        rowNum: 0,
        h2text: '考試日程',
        ptext: '領隊考試：109／5／9（六） 導遊考試：109／5／10（六）',
        btntext: '考程表下載',
        img: '',
        showImg: true,
        url: 'https://www.magee.tw/docs/exam_notes/48',
      },
      {
        rowNum: 1,
        h2text: '入場證下載',
        ptext: '考試通知書預定於109/4/24起開放考生自行下載。考試當日，應考生需持身分證件(國民身分證或護照或我國居留證) 入場應試。考試通知書預定於109/4/24起開放考生自行下載。考試當日，應考生需持身分證件(國民身分證或護照或我國居留證) 入場應試。',
        btntext: '考試通知書下載',
        img: '',
        showImg: true,
        url: 'https://register.moex2.nat.gov.tw/index.html',
      },
      {
        rowNum: 1,
        h2text: '考試日程',
        ptext: '領隊考試：109／5／9（六） 導遊考試：109／5／10（六）',
        btntext: '考程表下載',
        img: '',
        showImg: true,
        url: 'https://www.magee.tw/docs/exam_notes/48',
      },
    ]
  }, ],
  Content3: [{
    h1text: 'H1 主標題',
    info: [{
        h2text: '考試日程',
        ptext: '領隊考試：109／5／9（六）',
        btntext: '考程表下載',
        img: '',
        showImg: true,
        url: 'https://www.magee.tw/docs/exam_notes/48',
      },
      {
        h2text: '考試日程',
        ptext: '領隊考試：109／5／9（六）',
        btntext: '考程表下載',
        img: '',
        showImg: true,
        url: 'https://www.magee.tw/docs/exam_notes/48',
      },
      {
        h2text: '考試日程',
        ptext: '領隊考試：109／5／9（六）',
        btntext: '考程表下載',
        img: '',
        showImg: true,
        url: 'https://www.magee.tw/docs/exam_notes/48',
      }
    ]
  }],
  Content4: [{
    h1text: '考試日程',
    h2text: '領隊考試',
    litext: ['109/4/24', '109/4/24', '109/4/24'],
  }],
  collStatus: {
    collapse_logo: true,
    collapse_horizontal: false,
    collapse_content1: false,
    collapse_content2: false,
    collapse_content2_2: false,
    collapse_content3: false,
    collapse_footer: false,
  },
  html: '',
};

const actions = {
  setEditNow({ commit }, data) {
    commit('saveEditNow', data);
  },
  addContent2({ state, commit }, data) {
    commit();
  },
  addContent2_2({
    state,
    commit
  }, data) {
    // console.log(data);
    state.Content2_2[0].info.push({
      rowNum: data + 1,
      h2text: '',
      ptext: '',
      btntext: '',
      img: '',
      showImg: false,
      url: '',
    }, {
      rowNum: data + 1,
      h2text: '',
      ptext: '',
      btntext: '',
      img: '',
      showImg: false,
      url: '',
    });
  },
  deleteContent2_2({
    state,
    commit
  }, data) {
    let arr = state.Content2_2[0].info.filter(e => e.rowNum !== data);
    commit('saveContent2_2', arr)
    // console.log(arr);
  }
};

const mutations = {
  saveContent2_2(state, data) {
    state.Content2_2[0].info = data
  },
  saveEditNow(state, data) {
    state.editNow = data;
  },
  saveHtml(state, data) {
    state.html = data;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
