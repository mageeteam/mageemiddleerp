import Vue from 'vue';

import axios from 'axios';
import VueAxios from 'vue-axios';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faUserSecret,
  faTimesCircle,
  faCircle,
  faDatabase,
  faClock,
  faPlusSquare,
  faFileExcel,
  faCopy,
  faFileExport,
  faPlay,
  faStar,
  faEdit,
  faWindowClose,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import 'bootstrap';
import 'jquery';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import _ from 'lodash';
import CKEditor from '@ckeditor/ckeditor5-vue';
import { createPopper } from '@popperjs/core';
import moment from 'moment';
import VueMoment from 'vue-moment';
import Sticky from 'vue-sticky-directive';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import '@/style/style.scss';
import '@/style/newsletter.scss';
import '@/style/example.scss';

Vue.config.productionTip = false;

library.add(
  faUserSecret,
  faTimesCircle,
  faCircle,
  faDatabase,
  faClock,
  faPlusSquare,
  faFileExcel,
  faCopy,
  faFileExport,
  faPlay,
  faStar,
  faEdit,
  faWindowClose,
);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(Sticky);
Vue.use(VueAxios, axios, _);
Vue.use(BootstrapVue, IconsPlugin, createPopper, moment, VueMoment);
Vue.use(CKEditor);
Vue.use(require('vue-moment'));

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
