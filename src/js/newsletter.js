import newsHorizontal from '@/components/newsletter/newsHorizontal.vue';
import newsContent1 from '@/components/newsletter/newsContent1.vue';
import newsContent2 from '@/components/newsletter/newsContent2.vue';
import newsContent2_2 from '@/components/newsletter/newsContent2_2.vue';
import newsContent3 from '@/components/newsletter/newsContent3.vue';
import newsContent4 from '@/components/newsletter/newsContent4.vue';
import newsContent2Group from '@/components/newsletter/newsContent2Group.vue';
import newsContent2_2Group from '@/components/newsletter/newsContent2_2Group.vue';
import newsContent3Group from '@/components/newsletter/newsContent3Group.vue';
import newsFooterGroup from '@/components/newsletter/newsFooterGroup.vue';
import localStyle from '@/components/newsletter/localStyle.vue';
import globalStyle from '@/components/newsletter/globalStyle.vue';

export default {
  components: {
    newsHorizontal,
    newsContent1,
    newsContent2,
    newsContent2_2,
    newsContent3,
    newsContent4,
    newsContent2Group,
    newsContent2_2Group,
    newsContent3Group,
    newsFooterGroup,
    localStyle,
    globalStyle,
  },
  data() {
    return {
      Content2Group: 0,
      Content2_2Group: 0,
      Content3Group: 0,
      footerGroup: 0,
      showContent2_2: false,
      showContent2: false,
      showHorizontal: false,
      showContent1: false,
      showContent3: false,
      showContent4: false,
      collapse_logo: true,
      collapse_horizontal: false,
      view: 'local',
      Horizontal: {
        h1text: '考前重點解題複習',
        h2text: '4/21起開放報名，僅限30名',
      },
      Content1: {
        h1text: '| 參加辦法 |',
        ptext: '凡有參加109/2/20~2/29期間三科衝刺班者，即可免費參加本次活動。並開放「現場上課」及「雲端上課」，學員可擇一方式參加。',
        btntext: 'Button',
      },
      aaa: [],
      myhtml: '',
    };
  },
  mounted() {


  },
  methods: {
    exportHtml() {
      console.log(this.$refs);
      let aaa = '';
      // console.log(this.$refs.htmlall.children[0].childNodes[0].childNodes[1].childNodes.length);
      for (let i = 0; i < this.$refs.htmlall.children[0].childNodes[0].childNodes[1].childNodes
        .length; i++) {
        if (this.$refs.htmlall.children[0].childNodes[0].childNodes[1].childNodes[i].childNodes[
            1] !== undefined) {
          // console.log(this.$refs.htmlall.children[0].children[0].children[1].children[i].children[1].innerHTML);
          aaa = aaa + this.$refs.htmlall.children[0].children[0].children[1].children[i].children[1]
            .innerHTML;
        }

      }
      this.myhtml = `
            <!DOCTYPE html>
            <html>
            <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width,initial-scale=1.0">
            <title></title>

            </head>
            <body>
            ${aaa}
            </body>
            </html>`


      // this.myhtml = `<!--[if true]>
      //       <table>
      //           <tr>
      //               <td>
      //       <![endif]-->
      //       ${aaa}
      //       <!--[if true]>
      //               </td>
      //           </tr>
      //       </table>
      //       <![endif]-->
      //       `
      // this.html = aaa;
      console.log(this.myhtml);
      this.$store.commit('NewsLetter/saveHtml', this.myhtml);
      let changeType = {
        newLetterContent: 'newsletter1',
        newLetterContent2: 'newsletter2',
        newLetterContent3: 'newsletter3',
      };

      this.$router.push({
        name: 'newletterExport',
        params: {
          type: changeType[this.globalSt.switchStyle],
        }
      });

    },
    editNowNum(num) {
      this.$store.dispatch('NewsLetter/setEditNow', num);
    },
    showContent1_change(data) {
      if (data) {
        this.showContent1 = false;
      } else {
        this.showContent1 = true;
      }
    },
    showContent2_change(data) {
      if (data) {
        this.showContent2 = false;
      } else {
        this.showContent2 = true;
      }
    },
    showContent2_2_change(data) {
      if (data) {
        this.showContent2_2 = false;
      } else {
        this.showContent2_2 = true;
      }
    },
    newG(data) {
      this.Content2_2Group = data;
      console.log(`in newsletter${this.Content2_2Group}`);
    },
    changeView(name) {
      this.view = name;
    },
    upGroup2_2() {
      if (this.Content2_2Group > 0) {
        this.Content2_2Group = this.Content2_2Group - 1
      }
    },
    dnGroup2_2() {
      if (this.Content2_2[0].info.length - 1 > this.Content2_2Group) {
        this.Content2_2Group = this.Content2_2Group + 1
        console.log(this.Content2_2Group);
        console.log(this.Content2_2[0].info.length);
      }
    },
    upGroup2() {
      if (this.Content2Group > 0) {
        this.Content2Group = this.Content2Group - 1
      }
    },
    dnGroup2() {
      if (this.Content2[0].info.length - 1 > this.Content2Group) {
        this.Content2Group = this.Content2Group + 1
        console.log(this.Content2Group);
        console.log(this.Content2[0].info.length);
      }
    },
    upGroup3() {
      if (this.Content3Group > 0) {
        this.Content3Group = this.Content3Group - 1
      }
    },
    dnGroup3() {
      if (this.Content3[0].info.length - 1 > this.Content3Group) {
        this.Content3Group = this.Content3Group + 1
        console.log(this.Content3Group);
        console.log(this.Content3[0].info.length);
      }
    },
    dnFooterGroup() {
      if (this.newsFooter.info.length - 1 > this.footerGroup) {
        this.footerGroup = this.footerGroup + 1
        console.log(this.footerGroup);
        console.log(this.newsFooter.info.length);
      }
    },
    upFooterGroup() {
      if (this.footerGroup > 0) {
        this.footerGroup = this.footerGroup - 1
      }
    },
    addContent2_2(rowNum) {
      console.log(rowNum);
      if (rowNum >= 0) {
        this.$store.dispatch('NewsLetter/addContent2_2', rowNum);
      } else {
        console.log('失敗');
      }

    }
  },
  computed: {
    editNow: {
      get() {
        return this.$store.state.NewsLetter.editNow;
      }
    },
    // Content2_2Group2() {
    //   this.Content2_2Group = this.Content2_2[0].info.length;
    //   return this.Content2_2Group;
    // },
    Logo: {
      get() {
        return this.$store.state.NewsLetter.Logo;
      }
    },
    Content2: {
      get() {
        return this.$store.state.NewsLetter.Content2;
      }
    },
    Content2_2: {
      get() {
        return this.$store.state.NewsLetter.Content2_2;
      }
    },
    Content3: {
      get() {
        return this.$store.state.NewsLetter.Content3;
      }
    },
    globalSt: {
      get() {
        return this.$store.state.NewsLetter.style.globalStyle;
      }
    },
    localSt: {
      get() {
        return this.$store.state.NewsLetter.style.localStyle;
      }
    },
    Content4: {
      get() {
        return this.$store.state.NewsLetter.Content4;
      }
    },
    newsFooter: {
      get() {
        return this.$store.state.NewsLetter.newsFooter;
      }
    },
    collStatus: {
      get() {
        return this.$store.state.NewsLetter.collStatus;
      }
    }
  },
  name: 'newsletter',
};
