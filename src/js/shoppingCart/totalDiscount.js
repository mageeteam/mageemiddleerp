export default {
  data() {
    return {
      buyfulltype: 0,
      mytxt: false,
      memberLV: [{
        num: 1,
        level: '測試會員',
        disType: 0,
        dis: 9,
      },
      {
        num: 2,
        level: '黃金會員',
        disType: 1,
        dis: 1000,
      }],
      temp: {
        code: '',
        type: [0, ''],
        quota: [0, 0],
        enable: false,
        time_start: '',
        time_end: '',
        remarks: '',
      },
      CouponCode: [{
        code: '200401',
        type: [500, '7'],
        quota: [10, 2],
        enable: true,
        time_start: '2020／04／01',
        time_end: '2020／04／30',
        remarks: '',
      },
      {
        code: 'BK200',
        type: [400, '200'],
        quota: [50, 0],
        enable: true,
        time_start: '2020／03／01',
        time_end: '永遠',
        remarks: '',
      },
      ],
    };
  },
  methods: {
    is_enable(enable) {
      if (enable) {
        return '是';
      }
      return '否';
    },
    show() {
      if (this.mytxt) {
        this.mytxt = false;
      } else {
        this.mytxt = true;
      }
      console.log(this.mytxt);
    },
    edit(item) {
      this.temp = {};
      this.temp = {
        ...item,
      };
    },
    newAdd() {
      this.temp = {
        code: '',
        type: [0, ''],
        quota: [0, 0],
        enable: false,
        time_start: '',
        time_end: '',
        remarks: '',
      };
    },
    newAddRow() {
      // this.CouponCode.push({
      //   code: this.temp.code,
      //   type: this.temp.type,
      //   quota: this.temp.quota,
      //   enable: this.temp.enable,
      //   time_start: this.temp.time_start,
      //   time_end: this.temp.time_end,
      //   remarks: this.temp.remarks,
      // });
      console.log(this.temp);
      this.CouponCode.push(this.temp);
    },
  },

  computed: {
    discountRange() {
      const discountRange = [];
      for (let i = 9.9; i > 0.2; i -= 0.1) {
        discountRange.push({
          value: i.toFixed(1),
          text: i.toFixed(1),
        });
      }
      return discountRange;
    },
    dis_code_list: {
      get() {
        return this.$store.state.discount.dis_code_list;
      },
    },
  },
};
