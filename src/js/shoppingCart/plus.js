export default {
  data() {
    return {
      discountType: 'discount_price',
      discountType_modal: 'discount_price',
      showType: 'showFull',
      isEdit: false,
    };
  },
  methods: {
    toNew() {
      this.$router.push('/shoppingCart/newDiscount');
    },
    edit() {
      if (this.isEdit !== false) {
        this.isEdit = false;
      } else {
        this.isEdit = true;
      }
      // console.log(this.isEdit);
    },
    editTempData(item) {
      this.$store.commit('discount/change_edit_dis_list', item);
    },
  },
  mounted() {
    console.log(this.buyfullDisData);
  },
  computed: {
    discountRange() {
      const discountRange = [];
      for (let i = 9.9; i > 0.2; i -= 0.1) {
        discountRange.push({
          value: i.toFixed(1),
          text: i.toFixed(1),
        });
      }
      return discountRange;
    },
    buyfullDisData: {
      get() {
        return this.$store.state.discount.dis_list;
      },
    },
    lang: {
      get() {
        return this.$store.state.Public.langtext;
      },
    },
    edit_dis_list: {
      get() {
        return this.$store.state.discount.edit_dis_list;
      },
    },
  },
};
