import _ from 'lodash';

export default {
  data() {
    return {
      // discountType: 'discount_price',
      // showType: 'showFull',
      dis_temp_data: {},
    };
  },
  methods: {
    toExtraSetting() {
      this.$router.push('/shoppingCart/plus');
    },
    toFulldis() {
      this.$router.push('/shoppingCart/fullDiscount');
    },
  },
  computed: {
    lang: {
      get() {
        return this.$store.state.Public.langtext;
      },
    },
    discountRange() {
      const discountRange = [];
      for (let i = 9.9; i > 0.2; i -= 0.1) {
        discountRange.push({
          value: i.toFixed(1),
          text: i.toFixed(1),
        });
      }
      return discountRange;
    },
    buyfullDisData: {
      get() {
        return this.$store.state.discount.buyfullDisData;
      },
    },
  },
  created() {
    this.dis_temp_data = _.cloneDeep(this.buyfullDisData[0]);
  },
};
