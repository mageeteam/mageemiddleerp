export default {
  data() {
    return {
      isOverview: false,
      setWidth: '',
    };
  },
  methods: {
    changeSort(num) {
      this.$store.commit('productManage/changeProSort', num);
    },
  },
  computed: {
    proSort: {
      get() {
        return this.$store.state.productManage.proSort;
      },
    },
    proOverViewWidth: {
      get() {
        return this.$store.state.productManage.proOverViewWidth;
      },
    },
  },
};
